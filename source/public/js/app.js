/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

window.onload = function () {
    //Id password which will used for authentication on emotive server.
    var userId = 'mohitpawar10';
    var userPassword = 'javaISBEST@EMOTIVE';
    var clientId = 'S3KnCHHzV5HOZAYeZawLUCnEYOor7eKSys6tujGJ';
    var clientSecret = 'HlOAUaSPoR2eLREzwTadP3sUg7j72mATQ1kWYittWVBhiERfHfFkdfsQ8IKffSQ6HLmRnaaYWzCSt0jyFIHwPF90aT2izu39Ng2tebEf2tQMcPAgoAu8FSNKkj6R806z';
    //Buttons
    var loginBtn = $('#loginBtn');
    var logoutBtn = $('#logoutBtn');
    var loggedInUserBtn = $('#loggedInUserBtn');
    var getConnectedHeadset = $('#getConnectedHeadset');
    var authTokenBtn = $('#getAuthToken');
    var createSessionBtn = $('#createSession');
    var querySessionBtn = $('#querySession');
    var startRecordingBtn = $('#startRecording');
    var subscribeBtn = $('#subscribe');
    // var authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBJZCI6IiIsImV4cCI6MTUxMjEwNTk3MCwiZmlyc3ROYW1lIjoiIiwibGFzdE5hbWUiOiIiLCJsaWNlbnNlSWQiOiIiLCJsaWNlbnNlU2NvcGUiOjEsImxpY2Vuc2VfZXhwaXJlIjowLCJsaWNlbnNlX2hhcmRMaW1pdCI6MCwibGljZW5zZV9zb2Z0TGltaXQiOjAsIm5iZiI6MTUxMTg0Njc3MCwidXNlckNsb3VkSWQiOi0xLCJ1c2VybmFtZSI6IkJhc2ljIn0.GF2gMCgRvNTfcujYOp_TQM1hYUd8ZY4QuHYLLGJFwyg";
    var authToken = null;
    var response = null;
    var socket = new WebSocket('wss://emotivcortex.com:54321');

    subscribeBtn.click(function () {
        sendToServer(subscribeRequest(), socket);
    });

    startRecordingBtn.click(function () {
        sendToServer(startRecordingRequest(), socket);
    });

    querySessionBtn.click(function () {
        sendToServer(querySessionRequest(), socket);
    });

    createSessionBtn.click(function () {
        sendToServer(createSessionRequest(), socket);
    });

    loginBtn.click(function () {
        sendToServer(userLoginRequest(), socket);
    });

    logoutBtn.click(function () {
        sendToServer(userLogoutRequest(), socket);
    });

    loggedInUserBtn.click(function () {
        sendToServer(loggedInUserRequest(), socket);
    });

    getConnectedHeadset.click(function () {
        sendToServer(connectedHeadsetRequest(), socket);
    });

    authTokenBtn.click(function () {
        sendToServer(authTokenRequest(), socket);
    });

    getHeadsets(socket);

    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawChartPower);
    //Set them as Global so that we can redraw chart after getting new data from server.
    var data, options, chart;
    var data_power, options_power, chart_power;

    function drawChartPower() {

        // Create the data table.
        data_power = new google.visualization.DataTable();
        data_power.addColumn('number', 'Time');
        data_power.addColumn('number', 'Battery');
        data_power.addColumn('number', 'Signal');
        data_power.addColumn('number', 'AF3');
        data_power.addColumn('number', 'T7');
        data_power.addColumn('number', 'Pz');
        data_power.addColumn('number', 'T8');
        data_power.addColumn('number', 'AF4');

        // Set chart options
        options_power = {
            'title': 'Headset health and signal strength',
            'width': 1000,
            'height': 500,
            'animation': {
                duration: 1000,
                easing: 'out'
            },
            'vAxis': {
                'title': 'Power'
            },
            'hAxis': {
                'title': 'Time'
            }
        };

        // Instantiate and draw our chart, passing in some options.
        chart_power = new google.visualization.LineChart(document.getElementById('chart_div_power'));
        chart_power.draw(data_power, options_power);

        return chart_power;
    }

    function drawChart() {

        // Create the data table.
        data = new google.visualization.DataTable();
        data.addColumn('number', 'Time');
        data.addColumn('number', 'Interest');
        data.addColumn('number', 'Stress');
        data.addColumn('number', 'Relaxation');
        data.addColumn('number', 'Excitement');
        data.addColumn('number', 'Engagement');
        data.addColumn('number', 'Long term excitement');
        data.addColumn('number', 'Focus');

        // Set chart options
        options = {
            'title': 'Data from headset',
            'width': 1000,
            'height': 500,
            'animation': {
                duration: 1000,
                easing: 'out'
            },
            'vAxis': {
                'title': 'Emotions'
            },
            'hAxis': {
                'title': 'Time'
            },
            'is3D': true
        };

        // Instantiate and draw our chart, passing in some options.
        chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);

        return chart;
    }

    function updateChart(matrixData) {
        if ("met" in matrixData) {
            //console.log(matrixData['time']);
            data.addRow([matrixData['time'], Math.round(matrixData['met'][0] * 100, 0), Math.round(matrixData['met'][1] * 100, 0), Math.round(matrixData['met'][2] * 100, 0), Math.round(matrixData['met'][3] * 100, 0), Math.round(matrixData['met'][4] * 100, 0), Math.round(matrixData['met'][5] * 100, 0), Math.round(matrixData['met'][6] * 100, 0)]);

            chart.draw(data, options);
        }
        //data.addRows
    }

    function updateChartPower(matrixData) {
        if ("dev" in matrixData) {
            console.log(matrixData['dev'][2][1]);
            data_power.addRow([matrixData['time'], Math.round(matrixData['dev'][0] * -100, 0), Math.round(matrixData['dev'][1] * 100, 0), Math.round(matrixData['dev'][2][0] * 100, 0), Math.round(matrixData['dev'][2][1] * 100, 0), Math.round(matrixData['dev'][2][2] * 100, 0), Math.round(matrixData['dev'][2][3] * 100, 0), Math.round(matrixData['dev'][2][4] * 100, 0)]);

            chart_power.draw(data_power, options_power);
        }
        //data.addRows
    }

    function subscribe(socket) {
        console.log("Sending subscribe request");
        socket.send(JSON.stringify(subscribeRequest()));
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            console.log("video is playing : " + playing);
            // if (playing){
            updateChart(response);
            setTimeout(updateChartPower(response), 0);
            // }
        };
    }

    function createSession(socket) {
        console.log("Creating session");
        socket.send(JSON.stringify(createSessionRequest()));
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            //console.log(response.error.message);
            subscribe(socket);
            return response;
        };
    }

    function login(socket) {
        console.log("Logging into cortex");
        socket.send(JSON.stringify(userLoginRequest()));
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            //console.log(response.error.message);
            setAuthToken(socket);
            return response;
        };
    }

    function setAuthToken(socket) {
        console.log("Setting Auth token");
        socket.send(JSON.stringify(authTokenRequest()));
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            if (!("error" in response)) {
                if ("_auth" in response.result) authToken = response.result._auth;
                console.log("auth token set successfully");
                createSession(socket);
            }
            return response;
        };
    }

    function getHeadsets(socket) {
        console.log("Getting Available headsets");
        socket.onopen = function (event) {
            socket.send(JSON.stringify(connectedHeadsetRequest()));
            socket.onmessage = function (event) {
                console.log("Server says " + event.data);
                response = JSON.parse(event.data);
                //console.log(response.error.message);
                login(socket);
                return response;
            };
        };
    }

    function sendToServer(request, socket) {
        socket.send(JSON.stringify(request));
        // console.log("Request sent successfully");
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            return response;
        };
    }

    function getMessageFromServer(socket) {
        socket.onmessage = function (event) {
            console.log("Server says " + event.data);
            response = JSON.parse(event.data);
            //console.log(response.error.message);
            return response;
        };
    }

    function subscribeRequest() {
        return {
            method: "subscribe",
            jsonrpc: "2.0",
            params: {
                _auth: authToken,
                streams: ["met", "dev"]
            },
            id: 1
        };
    }

    function startRecordingRequest() {
        return {
            method: "updateSession",
            jsonrpc: "2.0",
            params: {
                _auth: authToken,
                status: "startRecord"
            },
            id: 1
        };
    }

    function querySessionRequest() {
        return {
            method: "querySessions",
            jsonrpc: "2.0",
            params: {
                _auth: authToken
            },
            id: 1
        };
    }

    function createSessionRequest() {
        return {
            method: "createSession",
            jsonrpc: "2.0",
            params: {
                _auth: authToken,
                status: "open"
            },
            id: 1
        };
    }

    function authTokenRequest() {
        return {
            method: "authorize",
            jsonrpc: "2.0",
            params: {},
            id: 1
        };
    }

    function userLoginRequest() {
        return {
            method: "login",
            jsonrpc: "2.0",
            params: {
                username: userId,
                password: userPassword,
                client_id: clientId,
                client_secret: clientSecret
            },
            id: 1
        };
    }

    function userLogoutRequest() {
        return {
            method: "logout",
            jsonrpc: "2.0",
            params: {
                username: userId
            },
            id: 1
        };
    }

    function loggedInUserRequest() {
        return {
            method: "getUserLogin",
            jsonrpc: "2.0",
            id: 1
        };
    }

    function connectedHeadsetRequest() {
        return {
            method: "queryHeadsets",
            jsonrpc: "2.0",
            params: {},
            id: 1
        };
    }
};

//var tag = document.createElement('script');

//tag.src = "http://www.youtube.com/iframe_api";
//var firstScriptTag = document.getElementsByTagName('script')[0];
//firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
//console.log('script loaded');

var player;
window.onYouTubeIframeAPIReady = function () {
    player = new YT.Player('player', {
        videoId: 's595S1Vf3PE',
        events: {
            'onReady': onReady
        }
    });
};

var playing = false;

function onReady() {
    player.addEventListener('onStateChange', function (e) {
        console.log('State is:', e.data);
        if (e.data == 1) {
            return playing = true;
        } else {
            return playing = false;
        }
    });
}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);