<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Websocket demo</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="http://www.youtube.com/iframe_api"></script>
</head>
<body>
<div class="container">
    <h1>
        Demo web socket connection to Emotive server.
    </h1>

    <h3>Add your text here</h3>

    <div>
        <button id="loginBtn">Login</button>
        <button id="logoutBtn">Logout</button>
        <button id="loggedInUserBtn">GetLoggedInUser</button>
        <button id="getConnectedHeadset">GetConnectedHeadset</button>
        <button id="getAuthToken">GetAuthToken</button>
        <button id="createSession">CreateSession</button>
        <button id="querySession">QuerySession</button>
        <button id="startRecording">StartRecording</button>
        <button id="subscribe">Subscribe</button>
    </div>
    <div class="response"></div>
    <div class="error"></div>
    <div id="player"></div>
    {{--<iframe id="player" width="560" height="315" src="https://www.youtube.com/embed/s595S1Vf3PE?rel=0"--}}
            {{--frameborder="0"--}}
            {{--allowfullscreen></iframe>--}}
    <div id="chart_div" class=""></div>
    <div id="chart_div_power" class=""></div>
</div>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>