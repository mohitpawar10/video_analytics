let mix = require('laravel-mix');
mix.browserSync('video_analytics.dev');
mix.disableSuccessNotifications();


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/assets/js/app.js'], 'public/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts('resources/assets/js/vendor/jquery.min.js', 'public/js/vendor.js')
    .options({
        processCssUrls: false
    });

mix.browserSync({
    proxy: 'video_analytics.dev'
});
